'use strict';

module.exports = (grunt) => {
  const spawn = require('cross-spawn');
  // const Hexo = require('hexo');
  const path = require('path');

  grunt.registerTask('docs:generate', 'Generate DUO UX Library documentation', function () {
    console.log(path.join(process.cwd(), grunt.config.get('hexo.source')));
    let done = this.async();

    let params = [
      'generate',
      '--cwd=./_docs/'
    ];

    if (grunt.option('debug')) {
      params.push('--debug');
    }

    let extension = /^win/.test(process.platform) ? '.cmd' : '';
    let hexo = spawn(`./_docs/node_modules/.bin/hexo${extension}`, params);

    hexo.stdout.on('data', data => {
      grunt.log.writeln([data]);
    });

    hexo.stderr.on('data', data => {
      grunt.log.error(data);
    });

    hexo.on('close', code => {
      console.log(`child process exited with code ${code}`);
      done();
    });
  });

  grunt.registerTask('docs:serve', 'Serve the documentation at localhost:5000', function () {
    let done = this.async();
    let params = [
      'serve',
      '--cwd=./_docs/',
      `--port=${grunt.config.get('pkg.port')}`
    ];

    if (grunt.option('debug')) {
      params.push('--debug');
    }

    let extension = /^win/.test(process.platform) ? '.cmd' : '';
    let hexo = spawn(`./_docs/node_modules/.bin/hexo${extension}`, params);

    hexo.stdout.on('data', data => {
      grunt.log.writeln([data]);
    });

    hexo.stderr.on('data', data => {
      grunt.log.error(data);
    });

    hexo.on('close', code => {
      console.log(`child process exited with code ${code}`);
      done();
    });
  });

  // /**
  //  * Initializes a hexo instance, reads the source dir from the config file
  //  * Returns a promise which resolves into a hexo instance
  //  *
  //  * @return Promise
  //  */
  // function getHexo() {
  //   let hexo = new Hexo(path.join(process.cwd(), grunt.config.get('hexo.source')), {});
  //
  //   return new Promise(resolve => {
  //     hexo.init().then(() => {
  //         resolve(hexo);
  //       },
  //       () => {
  //         throw new Error('Could not initialize hexo');
  //       }
  //     );
  //   });
  //
  // }

};
