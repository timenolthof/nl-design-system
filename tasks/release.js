module.exports = (grunt) => {
  "use strict";
  const child_process = require('child_process');
  const versions = [
    'major',
    'minor',
    'patch',
    'preminor',
    'prerelease',
    'premajor'
  ];

  let version = grunt.option('bump') || 'patch';
  const hardVersion = grunt.option('bump-to') || null;

  grunt.registerTask('release', 'Build a fresh version of the library', [
    'uno-bump',
    'uno-bump-commit',
    'build',
  ]);

  grunt.registerTask('uno-bump','Bump version', () => {
    if(versions.indexOf(version) === -1) {
      version = 'patch';
    }
    if(hardVersion) {
      grunt.option('setversion', hardVersion);
      grunt.task.run('bump-only')

    } else {
      grunt.task.run('bump-only:' + version);
    }

  });

  grunt.registerTask('uno-bump-commit','Bump version', () => {
    grunt.task.run('bump-commit');
  });
};