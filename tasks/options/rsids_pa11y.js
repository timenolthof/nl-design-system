'use strict';

let ignore = [
  // Check that the link text combined with programmatically determined link context identifies the purpose of the link.
  'WCAG2AA.Principle2.Guideline2_4.2_4_4.H77,H78,H79,H80,H81',
  // Check that the link text combined with programmatically determined link context, or its title attribute, identifies the purpose of the link.
  'WCAG2AA.Principle2.Guideline2_4.2_4_4.H77,H78,H79,H80,H81,H33',
  // Check that a change of context does not occur when this input field receives focus.
  'WCAG2AA.Principle3.Guideline3_2.3_2_1.G107'
];

let files = [
  'docs/componenten/**/*.html',
  'docs/core/**/*.html'
];

module.exports = {

  docs: {
    options: {
      rootElement: '#content',
      hideElements: 'code', // Hide all code examples
      reporter: 'console',
      debug: false,
      // By default, ignore contrast errors, since they're mostly disabled items,
      // to which the contrast rule does not apply
      ignore: [].concat(
        // This element has insufficient contrast at this conformance level. Expected a contrast ratio of at least 4.5:1
        'WCAG2AA.Principle1.Guideline1_4.1_4_3.G18',
        'WCAG2AA.Principle1.Guideline1_4.1_4_3.G18.Fail',
        ignore
      ),
      level: 'error',
      standard: 'WCAG2AA'
    },
    file: files

  },

  strict: {
    options: {
      rootElement: '#content',
      hideElements: 'code', // Hide all code examples
      reporter: 'console',
      debug: false,
      ignore: ignore,
      level: 'error',
      standard: 'WCAG2AA'
    },
    file: files
  }
};