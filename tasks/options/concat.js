'use strict';
const path = require('path');
const prc = require('process');

module.exports = {
  dist: {
    src: [
      'dist/core/base.css',
      'dist/core/grid.css',
      'dist/core/animations.css',
      'dist/components/**/*.css'
    ],
    options: {
      stripBanners: {
        block: true
      },
      process(src, file) {
        // Search for all ...: url(''); values
        return replaceUrls(src, file, false);

      }
    },
    dest: 'dist/core/uno.css'
  },

  tridion: {
    src: [
      'dist/core/base.css',
      'dist/core/grid.css',
      'dist/core/animations.css',
      'dist/components/**/*.css'
    ],
    options: {
      stripBanners: {
        block: true
      },
      process (src, file) {
        // Search for all ...: url(''); values
        return replaceUrls(src, file, true);

      }
    },
    dest: 'dist/core/uno-t.css'
  }

};

function replaceUrls(src, file, tridion) {
  src = src.replace(/url\(('|")?(\..*?)('|")?\)/g, (matches, _m1, url) => {
    const wrap = tridion ? ['[', ']'] : ["'", "'"];
    // Replaces relative urls in the source files to relative urls in
    // the concatenated file.
    // Example:
    // ./components/form/input.css contains a reference to an image:
    // ../../images/file.svg
    // When concatenating, this url becomes invalid,
    // so we change the url to ../images/file.svg

    // Get the path of the concatenated file
    let coreDir = `${prc.cwd()}/dist/core/`;

    // Get the path of the source file
    let fileDir = path.dirname(file) + '/';

    // Create a valid path based on the source file and the referenced file
    let urlPath = path.normalize(fileDir + url);

    // Change the path so it becomes relative to the concatenated file
    urlPath = path.relative(coreDir, urlPath)

    // Windows uses forward slashes in paths...
    urlPath = urlPath.split('\\').join('/');

    return `url(${wrap[0]}${urlPath}${wrap[1]})`;
  });
  return src.replace(/\/\*# sourceMappingURL.*?\*\//g, '');
}
