module.exports = {
  rootDir: "../",
  clearMocks: true,
  testMatch: [
    "**/test/**/**/*.test.[jt]s?(x)"
  ],
  collectCoverage: true,
  coverageDirectory: "coverage",
  collectCoverageFrom: [
    "**/src/**/*.[jt]s?(x)"
  ],
};