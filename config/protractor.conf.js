"use strict";
exports.config = {
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['../test/visual/**/*.js'],
  framework: 'jasmine',

  onPrepare: () => {
    browser.getCapabilities().then(function (cap) {
      browser.browserName = cap.caps_.browserName;
    });
  },

  multiCapabilities: [{
    'browserName': 'chrome'
  }, {
    'browserName': 'internet explorer'
  }],

  params: {
    viewports: [
      {
        name: 'desktop',
        width: 1200,
        height: 1024
      },
      {
        name: 'tablet-portrait',
        width: 768,
        height: 1024
      },
      {
        name: 'tablet-landscape',
        width: 1024,
        height: 768
      },
      {
        name: 'mobile-portrait',
        width: 320,
        height: 568
      },
      {
        name: 'mobile-landscape',
        width: 568,
        height: 320
      }
    ]
  }
};