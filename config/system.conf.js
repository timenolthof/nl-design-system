System.config({
    packages: {
        'uno': {
            defaultExtension: 'js'
        }
    },

    map: {'Promise': 'node_modules/promise-polyfill/promise.js'},

    'transpiler': 'typescript',
    'paths': {
        'systemjs': 'node_modules/systemjs/dist/system.js',
        'typescript': 'node_modules/typescript/lib/typescript.js'
    }

});